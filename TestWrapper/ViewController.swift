//
//  ViewController.swift
//  TestWrapper
//
//  Created by Anil Sharma on 10/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//
import UIKit
import KRCFramework


class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
//        let krcMainViewController =  KRCMainViewController()
        let krcMainViewController =  KRCRNViewController()
  self.navigationController?.pushViewController(krcMainViewController, animated: true)
    }

}
